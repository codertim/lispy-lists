(def lists {"Office" () "Home" ()})


(defn show-single-list [list-key list-map2]
	"show single list name(key)"
	(println "\n***** " list-key " *****")
	(def items-list (get list-map2 list-key))
	(doseq [item items-list]
		(println " - " item)))



(defn show-lists [list-map]
	"show all list names"
	(println list-map)
	(def formatted_list (keys list-map))
	(doseq [single-list formatted_list]
		(show-single-list single-list list-map)))



(defn show-list-names [all-lists]
	"Show the names of the lists"
	(def i 1)
	(doseq [list all-lists]
		(println (str " ** " i ": " (key list)))
		(def i (+ i 1))))



(defn show-current-list [current-list]
	"Display a chosen list"
	(println (str "\n\n*************** List: " (key current-list) " ***************"))
	(def seq-of-values (val current-list))
	(doseq [v seq-of-values]
		(println (str " - " v))
	)
	(println "*********************************************\n\n")
)



(defn show-selected-list [all-lists selected]
	"Display list details of list that user selected"
	(def i 0)
	(doseq [list all-lists]
		(def i (+ i 1))
		(if (= selected (str i))
			(show-current-list list))
	)
)



(defn selected-list? [the-user-input] 
	(cond
		 (= the-user-input "2") true
		 (= the-user-input "1") true
		 :else false)
)



(defn valid-list? [selected-list-val]
        "Check if list identifier is valid"
	; (def list-vals (range 1 (+ 1 (count lists))))
 	;(doseq [list-element list-vals]
	;	(println (str "Debugging - current list element: " list-element))
	;	(println (str "Debugging - current list element class: " (class list-element)))
	;)
	; (def list-val-strings (map #(format "%s" %) list-vals))
 	;(doseq [list-element-str list-val-strings]
	;	(println (str "Debugging - current list element as string: |" list-element-str "|")))

	; (some #{selected-list-val} list-val-strings)
	(some #{selected-list-val} (keys lists))
)



(defn add-element-to-list [mylist selected-list-val]
	(println "\nAdding element to list - enter list item:")
	; (println (str "  PARAM: selected-list-val=" selected-list-val))
	(def new-list-item (read-line))
	(println (str "\n  you entered: " new-list-item "\n"))
	(def selected-list (get lists selected-list-val))
	; (println (str "  debugging - selected list: " selected-list))
	(def selected-list (cons new-list-item selected-list))
	; (println (str "debugging - ew list with addition: " selected-list))
	(def lists (assoc lists selected-list-val selected-list))
	; (println (str "debugging - lists after assoc: " lists))
)



(defn add-to-list [] 
	(println "\n\nSelect a list to add to (enter name): ")
	(show-list-names lists)	
	(def selected-list-val (read-line))
	(if (valid-list? selected-list-val)
		(add-element-to-list lists selected-list-val)
		(println "\n\n\n ***** Invalid List ***** \n\n\n")
	)
	(println "\n\n\n")
)



(defn do-non-list-command [the-user-input]
	(println "Processing ...\n")
	(cond
		(= the-user-input "q") (System/exit 0)
		(= the-user-input "Q") (System/exit 0)
		(= the-user-input "a") (add-to-list)
		(= the-user-input "A") (add-to-list)
	)
)



(defn show-misc-options []
	(println " ** a: to add to a list")
	(println " ** q: to quit")
)



; setup lists - TODO: move to file
(def office_list '("paperwork" "clockout"))
(def home_list '("laundry" "magazines"))
(def lists (merge lists {"Office" office_list}))
(def lists (merge lists {"Home" home_list}))
;(println lists)
; (show-lists lists) TODO: call this function if user chooses to show all lists

(loop [i 0]
	(println "\nChoose a list to view: ")
	(show-list-names lists)
	(show-misc-options)
	(def user-input (read-line))
	; (println "\nYou chose: " user-input)
	(if (selected-list? user-input)
		(show-selected-list lists user-input)
		(do-non-list-command user-input))
	(recur (inc i)))

(println "\nDone.\n")
(flush)



